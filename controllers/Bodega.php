<?php

class Bodega extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $bodega_activa = ["tx_nombre" => 'BODEGUITAS'];
        $this->view->bodega_activa = $bodega_activa;
        $this->view->title = "Inicio";
        $this->view->render('bodega/index');
    }

    function list()
    {
        // $arrBodega = $this->_bodegaDB->getLis;
        // $this->view->arrBodega = $arrBodega;
        $arrBodegas = $this->model->listar();
        $this->view->arrBodegas = $arrBodegas;
        $this->view->title = "Listado de Bodegas";
        $this->view->render('bodega/list');
    }

    function create()
    {

        $bodega_activa = ["tx_nombre" => 'BODEGUITAS'];
        $this->view->bodega_activa = $bodega_activa;
        $this->view->title = "Inicio";
        $this->view->render('bodega/create');
    }


    public function insert()
    {
        // print_r($tx_nombre);
        $params = [];
        $params['tx_nombre'] = $_POST['tx_nombre'];
        $params['tx_direccion'] = $_POST['tx_direccion'];
        $ok = $this->model->agregar($params);
        print_r($ok);
        return json_encode($ok);

    }

    function update()
    { }


    function delete()
    { 
        $id = $_POST['id'];
        $ok = $this->model->eliminar($id);
        print_r($ok);

        return json_encode($ok);
    }
}





