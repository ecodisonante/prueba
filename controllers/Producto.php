<?php

class Producto extends Controller
{
    function __construct()
    {
        parent::__construct();
        $this->loadModel('Bodega');
    }

    function index()
    {
        $this->view->title = "Productos";
        $this->view->render('producto/index');
    }


    function list()
    {
        $bod = new BodegaModel();
        $arrBodegas = $bod->listar();
        $arrProductos = $this->model->listar();
        $this->view->arrBodegas = $arrBodegas;
        $this->view->arrProductos = $arrProductos;
        $this->view->title = "Listado de Productos";
        $this->view->render('producto/list');
    }

    function create()
    {
        $bod = new BodegaModel();
        $arrBodegas = $bod->listar();
        $this->view->arrBodegas = $arrBodegas;

        $this->view->title = "Agregar Productos";
        $this->view->render('producto/create');
    }


    public function insert()
    {
        // print_r($tx_nombre);
        $params = [];
        $params['tx_nombre'] = $_POST['tx_nombre'];
        $params['id_bodega'] = $_POST['id_bodega'];
        $params['nr_stock'] = $_POST['nr_stock'];
        $ok = $this->model->agregar($params);
        print_r($ok);
        return json_encode($ok);
    }

    function update()
    { }


    function delete()
    {
        $id = $_POST['id'];
        $ok = $this->model->eliminar($id);
        print_r($ok);

        return json_encode($ok);
    }
}
