<?php

class Model
{
    function __construct()
    {
        // $this->db = new Database();
        $this->db = new mysqli(HOST, DBUSER, DBPASS, DATABASE);
    }

    /**
     * Obtiene todos los elementos de la tabla
     * @return array
     */
    public function list($table)
    {
        // preparar consulta
        $query = " SELECT * FROM " . $table;
        $result = $this->db->query($query);

        if ($result->num_rows > 0) {
            // retornar array
            $list = [];
            while ($item = $result->fetch_object()) {
                $list[] = [
                    'id' => $item->id,
                    'tx_nombre' => $item->tx_nombre,
                    'tx_direccion' => $item->tx_direccion
                ];
            }
            return $list;
        } else {
            return null;
        }
    }

    /**
     * Agrega un elemento a la tabla
     * @return boolean
     */
    public function add($table, $params)
    {
        if (isset($params) && is_array($params)) {
            // Construir query con parametros
            $keys = '';
            $values = '';
            $separator = '';
            foreach ($params as $key => $value) {
                $keys .= $separator . $key;
                $values .= $separator . "'" . $value . "'";
                $separator = ',';
            }
            // TODO: Corregir por posible inyeccion de código
            $query = " INSERT INTO " . $table . "(" . $keys . ") VALUES(" . $values . ")";
            return $this->db->query($query); // boolean
        }
    }

    /**
     * Elimina un elemento de la tabla
     * @return boolean
     */
    public function delete($table, int $id)
    {
        if ($id > 0) {
            $query = " DELETE FROM " . $table . " WHERE id = " . $id;
            return $this->db->query($query); // boolean
        }
    }


    /**
     * Actualiza un elemento a la tabla
     * @return boolean
     */
    public function update($table, $params, int $id)
    {
        if (isset($params) && is_array($params)) {
            // Construir query con parametros
            $set = '';
            $separator = '';
            foreach ($params as $key => $value) {
                $set .= $separator . $key . "='" . $value . "'";
                $separator = ',';
            }

            // TODO: Corregir por posible inyeccion de código
            $query = " UPDATE " . $table . " SET " . $set . " WHERE id = " .  $id;
            return $this->db->query($query); // boolean
        }
    }
}
