<?php

/**
 * Obtiene ruta y dirige a los controladores
 */
class Router
{
    public function __construct()
    {

        $uri            = $_GET['url'];
        $uri            = rtrim($uri, '/');
        $uri            = explode('/', $uri);

        // Si no hay controlador enviar a bodega
        if (empty($uri[0])) {
            $uri[0] = 'Bodega';
        }

        $controllerFile = 'controllers/' . $uri[0] . '.php';

        // Llamar controlador
        if (file_exists($controllerFile)) {
            require_once $controllerFile;
            $controller = new $uri[0];
            $controller->loadModel($uri[0]); // Obtiene modelo

            // Llamar metodo
            if (isset($uri[1])) {
                if (isset($uri[2])) { // Tiene parametros
                    $controller->{$uri[1]}($uri[2]);
                } else {
                    $controller->{$uri[1]}();
                }
            } else { // No hay metodo, usar index
                $controller->index();
            }
        }
    }
}
