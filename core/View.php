<?php

class View
{
    function __construct()
    {
    }

    function render($name)
    {        
        require 'views/shared/header.php';
        require 'views/' . $name . '.php';
        require 'views/shared/footer.php';
    }
}
