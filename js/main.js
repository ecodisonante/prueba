const base_url = window.location.origin;

$(function () {

    $('#btnCrearBodega').click(function (e) {
        e.preventDefault();
        crearBodega();
    });


    $('#btnCrearProducto').click(function (e) {
        e.preventDefault();
        crearProducto();
    });

});


// --- BODEGA --- //

function crearBodega() {

    var data = {};
    var error = "";
    data['tx_nombre'] = $('#tx_nombre').val();
    data['tx_direccion'] = $('#tx_direccion').val();
    if (data['tx_nombre'].length == 0) { error += "<p>Ingrese el nombre correctamente</p>"; }
    if (data['tx_direccion'].length == 0) { error += "<p>Ingrese la direccion correctamente</p>"; }

    if (error == "") {
        $.ajax({
            type: "POST",
            url: base_url + "/prueba/Bodega/insert",
            data: data,
            success: function (response) {
                console.log(response);
                if (response) {
                    // mostrar mensaje
                    msg('Producto Agregado', 'success');

                    // volver al cerrar modal
                    $("body").on('mousedown click keydown', function () {
                        window.history.back();
                    });
                } else {
                    msg('No se pudo crear Producto', 'error');
                }
            }
        });
    } else {
        console.log(error);
        Swal.fire('Error', error, 'error');
    }
}


function delBodega(id) {
    $.post(base_url + '/prueba/Bodega/delete', { id: id }, function (response) {
        console.log(response);
        if (response) {
            msg('Bodega Eliminada', 'success');
            $("body").on('mousedown click keydown', function () {
                location.reload();
            });
        } else {
            msg('No se pudo eliminar Bodega', 'error');
        }
    });
}


// --- PRODUCTO --- //

function crearProducto() {
    var data = {};
    var error = "";
    data['tx_nombre'] = $('#tx_nombre').val();
    data['nr_stock'] = $('#nr_stock').val();
    data['id_bodega'] = $('#id_bodega option:selected').val();

    console.log(data);

    if (data['tx_nombre'].length == 0) { error += "<p>Ingrese el nombre</p>"; }
    if (data['id_bodega'] == 0) { error += "<p>Seleccione Bodega</p>"; }
    if (data['nr_stock'] == 0) { error += "<p>Ingrese la cantidad</p>"; }

    if (error == "") {
        $.ajax({
            type: "POST",
            url: base_url + "/prueba/Producto/insert",
            data: data,
            success: function (response) {
                console.log(response);
                if (response) {
                    // mostrar mensaje
                    msg('Bodega Agregada', 'success');

                    // volver al cerrar modal
                    $("body").on('mousedown click keydown', function () {
                        window.history.back();
                    });
                } else {
                    msg('No se pudo crear Bodega', 'error');
                }
            }
        });
    } else {
        console.log(error);
        Swal.fire('Error', error, 'error');
    }
}


function delProducto(id) {
    $.post(base_url + '/prueba/Producto/delete', { id: id }, function (response) {
        console.log(response);
        if (response) {
            msg('Producto Eliminado', 'success');
            $("body").on('mousedown click keydown', function () {
                location.reload();
            });
        } else {
            msg('No se pudo eliminar Producto', 'error');
        }
    });
}
















function msg(title, type) {
    Swal.fire({
        title: title,
        icon: type,
        showConfirmButton: false
    });

}
