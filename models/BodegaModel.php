<?php
class BodegaModel extends Model
{
    protected $table = "Bodega";

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        return $this->list($this->table);
    }

    public function agregar($params)
    {
        return $this->add($this->table, $params);
    }

    public function eliminar(int $id)
    {
        return $this->delete($this->table, $id);
    }

    public function actualizar($params, $id)
    {
        return $this->update($this->table, $params, $id);
    }
}
