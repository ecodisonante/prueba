<?php
class ProductoModel extends Model
{
    protected $table = "Producto";

    public function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        // traer lista con nombre de bodega
        $query = " SELECT prod.*, bod.tx_nombre AS tx_bodega 
                   FROM Producto prod 
                   JOIN Bodega bod ON prod.id_bodega = bod.id";

        $result = $this->db->query($query);

        if ($result->num_rows > 0) {
            $list = [];
            while ($item = $result->fetch_object()) {
                $list[] = [
                    'id' => $item->id,
                    'tx_nombre' => $item->tx_nombre,
                    'tx_bodega' => $item->tx_bodega,
                    'nr_stock'  => $item->nr_stock,
                    'id_bodega' => $item->id_bodega
                ];
            }
            return $list;
        } else {
            return null;
        }
    }

    public function agregar($params)
    {
        return $this->add($this->table, $params);
    }

    public function eliminar(int $id)
    {
        return $this->delete($this->table, $id);
    }

    public function actualizar($params, $id)
    {
        return $this->update($this->table, $params, $id);
    }
}
