<div class="row">
    <div class="col-md-3 mx-auto">
        <form>
            <div class="form-group">
                <label for="tx_nombre">Nombre Bodega</label>
                <input type="text" class="form-control" id="tx_nombre" name="tx_nombre">
            </div>
            <div class="form-group">
                <label for="tx_direccion">Dirección</label>
                <input type="text" class="form-control" id="tx_direccion" name="tx_direccion">
            </div>
            <button type="submit" class="btn btn-primary" id="btnCrearBodega">Crear</button>
        </form>



    </div>
</div>