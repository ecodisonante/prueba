<div class="row text-center">
    <div class="col-md-3 mx-auto">
        <a href="<?= BASE_URL ?>Bodega/list">

            <div class="card bg-light">
                <div class="card-body">
                    <span>
                        <i class="fa fa-list fa-3x"></i>
                    </span>
                    <h4>
                        Ver Bodegas
                    </h4>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mx-auto">
        <a href="<?= BASE_URL ?>Bodega/create">
            <div class="card bg-light">
                <div class="card-body">
                    <span>
                        <i class="fa fa-plus fa-3x"></i>
                    </span>
                    <h4>
                        Agregar Bodega
                    </h4>
                </div>
            </div>
        </a>
    </div>
</div>