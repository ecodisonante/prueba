<div class="row">
    <div class="col-md-10 mx-auto">
        <table class="table table-hover table-striped table-bordered mt-3">

            <thead>
                <tr>
                    <th>#</th>
                    <th>Bodega</th>
                    <th>Dirección</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>

                <?php if (isset($this->arrBodegas) && sizeof($this->arrBodegas) > 0) {
                    foreach ($this->arrBodegas as $key => $value) { ?>
                        <tr>
                            <td><?= ($key + 1) ?></td>
                            <td><?= $value['tx_nombre'] ?></td>
                            <td><?= $value['tx_direccion'] ?></td>
                            <td>
                                <span class="btn btn-sm btn-danger" onclick="delBodega(<?=$value['id']?>)"><i class="fa fa-trash"></i> Eliminar</span>
                                <span class="btn btn-sm btn-warning" onclick="updBodega(<?=$value['id']?>)"><i class="fa fa-edit"></i> Actualizar</span>
                                <!-- <span class="btn btn-sm btn-success" onclick="selBodega($value['id'])"><i class="fa fa-check"></i> Seleccionar</span> -->
                            </td>
                        </tr>
                    <?php }
                    } else { ?>
                    <tr>
                        <td colspan="3" class="text-center">No hay bodegas</td>
                    </tr>
                <?php } ?>

            </tbody>


        </table>


    </div>
</div>