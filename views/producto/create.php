<div class="row">
    <div class="col-md-3 mx-auto">
        <form>
            <div class="form-group">
                <label for="tx_nombre">Nombre Producto</label>
                <input type="text" class="form-control" id="tx_nombre" name="tx_nombre">
            </div>
            <div class="form-group">
                <label for="id_bodega">Bodega</label>
                <select name="id_bodega" id="id_bodega" class="form-control">
                    <option value="0">--seleccione--</option>
                    <?php 
                    if (isset($this->arrBodegas)) {
                        foreach ($this->arrBodegas as $item) { ?>
                            <option value="<?= $item['id'] ?>"><?= $item['tx_nombre'] ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="nr_stock">Stock</label>
                <input type="number" class="form-control" id="nr_stock" name="tx_direccion">
            </div>
            <button type="submit" class="btn btn-primary" id="btnCrearProducto">Crear</button>
        </form>
    </div>
</div>