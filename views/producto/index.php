<div class="row text-center">
    <div class="col-md-3 mx-auto">
        <a href="<?= BASE_URL ?>Producto/list">

            <div class="card bg-light">
                <div class="card-body">
                    <span>
                        <i class="fa fa-list fa-3x"></i>
                    </span>
                    <h4>
                        Ver Productos
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-3 mx-auto">
        <a href="<?= BASE_URL ?>Producto/create">
            <div class="card bg-light">
                <div class="card-body">
                    <span>
                        <i class="fa fa-plus fa-3x"></i>
                    </span>
                    <h4>
                        Agregar Productos
                    </h4>
                </div>
            </div>
        </a>
    </div>
</div>