<div class="row">
    <div class="col-md-10 mx-auto">
        <form>
            <div class="input-group">
                <input id="buscaProucto" type="text" placeholder="Buscar Producto" class="form-control">
                <select name="buscaBodega" id="buscaBodega" class="form-control">
                    <option value="0">--Filtrar por Bodega--</option>
                    <?php
                    if (isset($this->arrBodegas)) {
                        foreach ($this->arrBodegas as $item) { ?>
                            <option value="<?= $item['id'] ?>"><?= $item['tx_nombre'] ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </div>

        </form>
        <table class="table table-hover table-striped table-bordered mt-3">

            <thead>
                <tr>
                    <th>#</th>
                    <th>Producto</th>
                    <th>Bodega</th>
                    <th>Stock</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>

                <?php if (isset($this->arrProductos) && sizeof($this->arrProductos) > 0) {
                    foreach ($this->arrProductos as $key => $value) { ?>
                        <tr>
                            <td><?= ($key + 1) ?></td>
                            <td><?= $value['tx_nombre'] ?></td>
                            <td><?= $value['tx_bodega'] ?></td>
                            <td><?= $value['nr_stock'] ?></td>
                            <td>
                                <span class="btn btn-sm btn-danger" onclick="delProducto(<?= $value['id'] ?>)"><i class="fa fa-trash"></i> Eliminar</span>
                                <span class="btn btn-sm btn-warning" onclick="updProducto(<?= $value['id'] ?>)"><i class="fa fa-edit"></i> Actualizar</span>
                            </td>
                        </tr>
                    <?php }
                    } else { ?>
                    <tr>
                        <td colspan="4" class="text-center">No hay productos</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>